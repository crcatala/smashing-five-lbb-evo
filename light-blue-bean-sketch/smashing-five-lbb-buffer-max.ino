#include <ByteBuffer.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>

const String        BEAN_NAME                  = "SmashingFive1";
const uint8_t       SCRATCH_BANK_1             = 1;
const uint8_t       SCRATCH_BANK_2             = 2;
const uint8_t       SCRATCH_BANK_3             = 3;
const uint8_t       SCRATCH_BANK_4             = 4;
const uint8_t       SCRATCH_BANK_5             = 5;
const unsigned int  ACCELERATION_READ_INTERVAL = 2500; // 400Hz
const uint8_t       ACCELERATION_RANGE         = 16;
const bool          WAKE_ON_CONNECT            = true;
const uint8_t       CBUF_CAPACITY              = 99;
const uint8_t       CENTER_INDEX               = CBUF_CAPACITY/2;
const uint8_t       THRESHOLD_MAGNITUDE        = 70;
const unsigned int  SCRATCH_MAX_BYTES          = 20;
const double        ACCEL_SCALE_FACTOR         = 0.94;

Adafruit_ADXL345_Unified accel                 = Adafruit_ADXL345_Unified(12345);

AccelerationReading acceleration;
ByteBuffer          cbuf;

unsigned int i;
uint8_t      result1[20];
uint8_t      result2[20];
uint8_t      result3[20];
uint8_t      result4[20];
uint8_t      result5[20];
bool         connected;
bool         accelConnected;
uint8_t      magnitude;
uint8_t      currentVal;
uint8_t      nextVal;
uint8_t      previousVal;

void setup() {
  Bean.setBeanName(BEAN_NAME);
  Bean.enableWakeOnConnect(WAKE_ON_CONNECT);
  Bean.setAccelerationRange(ACCELERATION_RANGE);
  cbuf.init(CBUF_CAPACITY);
  accelConnected = accel.begin();
  accel.setRange(ADXL345_RANGE_16_G);
  accel.setDataRate(ADXL345_DATARATE_400_HZ);
}

void loop() {
  connected    = Bean.getConnectionState();

  if(connected && accelConnected) {
    sensors_event_t event; 
    accel.getEvent(&event);
    magnitude = (sqrt(pow(event.acceleration.x,2) + pow(event.acceleration.y,2) + pow(event.acceleration.z,2)))*ACCEL_SCALE_FACTOR;

    cbuf.push(magnitude);
    if (cbuf.getSize() >= cbuf.getCapacity()) {
      currentVal = cbuf.peek(CENTER_INDEX);
      if (currentVal > THRESHOLD_MAGNITUDE) {
        previousVal = cbuf.peek(CENTER_INDEX-1);
        nextVal     = cbuf.peek(CENTER_INDEX+1);
        if ((currentVal > previousVal) && (currentVal > nextVal)){
          for(i=0; i<SCRATCH_MAX_BYTES; i++)
          {
            result1[i] = cbuf.peek(i);
            result2[i] = cbuf.peek(i+20);
            result3[i] = cbuf.peek(i+40);
            result4[i] = cbuf.peek(i+60);
            result5[i] = cbuf.peek(i+80);
          }
          Bean.setScratchData(SCRATCH_BANK_1, result1, SCRATCH_MAX_BYTES);
          Bean.setScratchData(SCRATCH_BANK_2, result2, SCRATCH_MAX_BYTES);
          Bean.setScratchData(SCRATCH_BANK_3, result3, SCRATCH_MAX_BYTES);
          Bean.setScratchData(SCRATCH_BANK_4, result4, SCRATCH_MAX_BYTES);
          Bean.setScratchData(SCRATCH_BANK_5, result5, SCRATCH_MAX_BYTES-1);
          cbuf.clear();
        }
      }
    }
    
    delayMicroseconds(ACCELERATION_READ_INTERVAL);
  }
  else {
    cbuf.clear();
    Bean.sleep(0xFFFFFFFF);
  }
}