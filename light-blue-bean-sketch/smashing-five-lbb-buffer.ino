#include <ByteBuffer.h>

ByteBuffer cbuf;

const String        BEAN_NAME                  = "SmashingFive1";
const uint8_t       ACCELERATION_SCRATCH_BANK  = 2;
const unsigned long ACCELERATION_READ_INTERVAL = 10;
const uint8_t       ACCELERATION_RANGE         = 16;
const bool          WAKE_ON_CONNECT            = true;
const uint8_t       CBUF_CAPACITY              = 19;
const uint8_t       CENTER_INDEX               = CBUF_CAPACITY/2;
const uint8_t       THRESHOLD_MAGNITUDE        = 110;

uint8_t accelBuffer[CBUF_CAPACITY];

void setup() {
  Bean.setBeanName(BEAN_NAME);
  Bean.enableWakeOnConnect(WAKE_ON_CONNECT);
  Bean.setAccelerationRange(ACCELERATION_RANGE);
  cbuf.init(CBUF_CAPACITY);
}

void loop() {
  bool connected = Bean.getConnectionState();
  AccelerationReading acceleration = Bean.getAcceleration();
  uint8_t magnitude;

  if(connected) {
    // Note we divide by 4 to decrease resolution to 0.125g/unit which fits into a byte
    magnitude = (sqrt(pow(acceleration.xAxis,2) + pow(acceleration.yAxis,2) + pow(acceleration.zAxis,2)))/4;
    cbuf.push(magnitude);
    if (cbuf.getSize() >= cbuf.getCapacity()) {
      uint8_t currentVal = cbuf.peek(CENTER_INDEX);
      if (currentVal > THRESHOLD_MAGNITUDE) {
        uint8_t previousVal = cbuf.peek(CENTER_INDEX-1);
        uint8_t nextVal     = cbuf.peek(CENTER_INDEX+1);
        if ((currentVal > previousVal) && (currentVal > nextVal)){
          for(int i=0; i<CBUF_CAPACITY; i++)
          {
            accelBuffer[i] = cbuf.peek(i);
          }
          Bean.setScratchData(ACCELERATION_SCRATCH_BANK, accelBuffer, CBUF_CAPACITY);
          cbuf.clear();
        }
      }
    }
    
    delay(ACCELERATION_READ_INTERVAL);
  }
  else {
    cbuf.clear();
    Bean.sleep(0xFFFFFFFF);
  }
}