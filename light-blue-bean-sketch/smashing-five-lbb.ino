const String        BEAN_NAME                  = "SmashingFive1";
const uint8_t       ACCELERATION_SCRATCH_BANK  = 2;
const unsigned long ACCELERATION_READ_INTERVAL = 10;
const uint8_t       ACCELERATION_RANGE         = 16;
const bool          WAKE_ON_CONNECT            = true;

void setup() {
	Bean.setBeanName(BEAN_NAME);
	Bean.enableWakeOnConnect(WAKE_ON_CONNECT);
	Bean.setAccelerationRange(ACCELERATION_RANGE);
}

void loop() {
	bool connected = Bean.getConnectionState();

	if(connected) {
		uint8_t bufferLength = 6;
		uint8_t accelBuffer[bufferLength];
		AccelerationReading acceleration = Bean.getAcceleration();
		
		accelBuffer[0] = acceleration.xAxis & 0xFF;
		accelBuffer[1] = acceleration.xAxis >> 8;
		accelBuffer[2] = acceleration.yAxis & 0xFF;
		accelBuffer[3] = acceleration.yAxis >> 8;
		accelBuffer[4] = acceleration.zAxis & 0xFF;
		accelBuffer[5] = acceleration.zAxis >> 8;

		Bean.setScratchData(ACCELERATION_SCRATCH_BANK, accelBuffer, bufferLength);
		
		delay(ACCELERATION_READ_INTERVAL);
	}
	else {
		Bean.sleep(0xFFFFFFFF);
	}
}

