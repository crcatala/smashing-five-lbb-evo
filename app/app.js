document.addEventListener('deviceready', function() { app.initialize() }, false);
document.getElementById('BLEButton').onclick=function(){ app.connect()};

var app = {};

app.UUID_SCRATCHSERVICE = 'a495ff20-c5b1-4b44-b512-1370f02d74de';

app.getScratchCharacteristicUUID = function(scratchNumber)
{
	return ['a495ff21-c5b1-4b44-b512-1370f02d74de',
		'a495ff22-c5b1-4b44-b512-1370f02d74de',
		'a495ff23-c5b1-4b44-b512-1370f02d74de',
		'a495ff24-c5b1-4b44-b512-1370f02d74de',
		'a495ff25-c5b1-4b44-b512-1370f02d74de'][scratchNumber - 1];
};

app.buffer = new CBuffer(51);
app.peakCount = 0;
app.THRESHOLD_MAGNITUDE = 280;

var accelScore   = 0;
var accelData1   = [];
var accelData2   = [];
var accelData3   = [];
var accelData4   = [];
var accelData5   = [];
var bank1HasData = false;
var bank2HasData = false;
var bank3HasData = false;
var bank4HasData = false;
var bank5HasData = false;
var accelIdeal = 140.0;
var scoreMax   = 100;
var score;
var highFiveCount = 0;
if ((localStorage["highFiveCount"] == undefined)) {
	console.log("No local storage highFiveCount, resetting...");
	localStorage["highFiveCount"] = "0";
}
$('#high-five-count').html(localStorage["highFiveCount"]);

	var chart = c3.generate({
		bindto: '#chart',
	  data: {
	    columns: [
	      ['data1', 0],
	    ],
	    type: 'area-spline'
	  }
	});

$('#reset-high-five-count').click(function(e) {
	e.preventDefault();
	console.log("attempting to reset count...");
	$('.small.test.modal').modal('show');
});

$('#confirm-reset').click(function(e) {
	localStorage["highFiveCount"] = "0";
	$('#high-five-count').html(localStorage["highFiveCount"]);
});

var processAccelData = function (bank) {
	console.log("processing for bank " + bank);
	switch(bank) {
    case 1:
      bank1HasData = true;
      break;
    case 2:
      bank2HasData = true;
      break;
    case 3:
      bank3HasData = true;
      break;
    case 4:
      bank4HasData = true;
      break;
    case 5:
      bank5HasData = true;
      break;                        
  }
  if (allBanksHaveHaveData()) {
  //   var accelData = [].concat(accelData1, accelData2, accelData3, accelData4, accelData5);
  //   console.log("All data received!");
		// for(var i=0; i<accelData.length; i++) {
  //   	console.log(i + ": " + accelData[i]);
  //   }
  	console.log("high five detected!")
    // console.log(data.score);
    // document.querySelector("#score").innerHTML = data.score;
    // columnData = ['data1'];
  	var columnData = ['data1'].concat(accelData1, accelData2, accelData3, accelData4, accelData5);
    for(var i=0; i<columnData.length; i++) {
      console.log(i + ": " + columnData[i]);
    }
    chart.load({
	  	unload:  ['data1'],
	    columns: [columnData]
		});
		$('#high-five-score').html(score);
		highFiveCount+=1;
		localStorage["highFiveCount"] = parseInt(localStorage["highFiveCount"]) + 1;
		$('#high-five-count').html(localStorage["highFiveCount"]);
		// $('#high-five-count').html(highFiveCount);
    resetBankFlags();
  }
};

var allBanksHaveHaveData = function () {
  return (bank1HasData && bank2HasData && bank3HasData && bank4HasData && bank5HasData);
};

var resetBankFlags = function () {
  bank1HasData = false;
  bank2HasData = false;
  bank3HasData = false;
  bank4HasData = false;
  bank5HasData = false;
};

app.initialize = function()
{
	app.connected = false;
};

app.deviceIsLightBlueBeanWithBleId = function(device, bleId)
{
	return ((device != null) && (device.name != null) && (device.name == bleId));
};

app.connect = function(user)
{
	var BLEId = document.getElementById('BLEId').value;

	app.showInfo('Trying to connect to "' + BLEId + '"');

	app.disconnect(user);

	function onScanSuccess(device)
	{
		function onConnectSuccess(device)
		{
			function onServiceSuccess(device)
			{
				// Update user interface
				app.showInfo('Connected to <i>' + BLEId + '</i>');
				document.getElementById('BLEButton').innerHTML = 'Disconnect';
				document.getElementById('BLEButton').onclick = new Function('app.disconnect()');
				document.getElementById('acceleration-display').style.display = 'block';

				// Application is now connected
				app.connected = true;
				app.device = device;

				app.notify(1, function (data) {
					var accelData = new Uint8Array(data);
					for(var i=0; i<accelData.length; i++) {
	          accelData1[i] = accelData[i];
        	}
	        processAccelData(1);
				});

				app.notify(2, function (data) {
					var accelData = new Uint8Array(data);
					for(var i=0; i<accelData.length; i++) {
	          accelData2[i] = accelData[i];
        	}
	        processAccelData(2);
				});

				app.notify(3, function (data) {
					var accelData = new Uint8Array(data);
					for(var i=0; i<accelData.length; i++) {
	          accelData3[i] = accelData[i];
			    	// console.log(i + ": " + accelData[i]);
        	}
        	score = scoreMax * Math.exp(-Math.pow((accelData[9] - accelIdeal)/10,2)/(2*(16)));
        	score = parseInt(score);
	        processAccelData(3);
				});

				app.notify(4, function (data) {
					var accelData = new Uint8Array(data);
					for(var i=0; i<accelData.length; i++) {
	          accelData4[i] = accelData[i];
        	}
	        processAccelData(4);
				});

				app.notify(5, function (data) {
					var accelData = new Uint8Array(data);
					for(var i=0; i<accelData.length; i++) {
	          accelData5[i] = accelData[i];
        	}
	        processAccelData(5);
				});

				// Create an interval timer to periocally read acceleration.
				// app.interval = setInterval(function() { app.readAcceleration(); }, 20);
			}

			function onServiceFailure(errorCode)
			{
				// Show an error message to the user
				app.showInfo('Error reading services: ' + errorCode);
			}

			// Connect to the appropriate BLE service
			device.readServices(
				[app.UUID_SCRATCHSERVICE],
				onServiceSuccess,
				onServiceFailure);
		};

		function onConnectFailure(errorCode)
		{
			// Show an error message to the user
			app.showInfo('Error ' + errorCode);
		}

		console.log('Found device: ' + device.name);

		// Connect if we have found a LightBlue Bean with the name from input (BLEId)
		var found= app.deviceIsLightBlueBeanWithBleId(
			device,
			document.getElementById('BLEId').value);
		if (found)
		{
			// Update user interface
			app.showInfo('Found "' + device.name + '"');

			// Stop scanning
			evothings.easyble.stopScan();

			// Connect to our device
			app.showInfo('Identifying service for communication');
			device.connect(onConnectSuccess, onConnectFailure);
		}
	}

	function onScanFailure(errorCode)
	{
		// Show an error message to the user
		app.showInfo('Error: ' + errorCode);
		evothings.easyble.stopScan();
	}

	// Update the user interface
	app.showInfo('Scanning...');

	// Start scanning for devices
	evothings.easyble.startScan(onScanSuccess, onScanFailure);
};

app.disconnect = function(user)
{
	// If timer configured, clear.
	if (app.interval)
	{
		clearInterval(app.interval);
	}

	app.connected = false;
	app.device = null;

	// Hide user inteface
	document.getElementById('acceleration-display').style.display = 'none';

	// Stop any ongoing scan and close devices.
	evothings.easyble.stopScan();
	evothings.easyble.closeConnectedDevices();

	// Update user interface
	app.showInfo('Not connected');
	document.getElementById('BLEButton').innerHTML = 'Connect';
	document.getElementById('BLEButton').onclick = new Function('app.connect()');
};

app.readAcceleration = function()
{
	function onDataReadSuccess(data)
	{
		var accelerationData = new Uint8Array(data);
		var x = evothings.util.littleEndianToInt16(accelerationData, 0)
		var y = evothings.util.littleEndianToInt16(accelerationData, 2)
		var z = evothings.util.littleEndianToInt16(accelerationData, 4)

		document.getElementById('acceleration-x-value').innerHTML = 'x: ' + x;
		document.getElementById('acceleration-y-value').innerHTML = 'y: ' + y;
		document.getElementById('acceleration-z-value').innerHTML = 'z: ' + z;

		// For 16g setting, 32 units == 1G.
		var magnitude = Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
		app.buffer.push(magnitude);
		if (app.buffer.size >= app.buffer.length) {
			var average = (app.buffer.get(24) + app.buffer.get(25) + app.buffer.get(26))/3;
			if (average > app.THRESHOLD_MAGNITUDE) {
	      app.peakCount +=1;
				document.getElementById('peak-count').innerHTML = app.peakCount;
				document.getElementById('peak-acceleration').innerHTML = (average*31.25e-3).toFixed(2) + " Gs";
				app.buffer.empty();
			}
		}
	}

	function onDataReadFailure(errorCode)
	{
		console.log('Failed to read acceleration with error: ' + errorCode);
		app.disconnect();
	}

	app.readDataFromScratch(2, onDataReadSuccess, onDataReadFailure);
};

app.writeDataToScratch = function(scratchNumber, data, succesCallback, failCallback)
{
	if (app.connected)
	{
		console.log('Trying to write data to scratch ' + scratchNumber);
		app.device.writeCharacteristic(
			app.getScratchCharacteristicUUID(scratchNumber),
			data,
			succesCallback,
			failCallback);
	}
	else
	{
		console.log('Not connected to device, cant write data to scratch.');
	}
};

app.notify = function (scratchBank, onDataCallback) {
	console.log("Attempting to enable notifications on bank " + scratchBank + "...");
	var onSuccess = function (data) {
		console.log("Data received on bank " + scratchBank);
		if (onDataCallback) { onDataCallback(data);}
	};

	var onFail = function (errorCode) {
		console.log('enableNotification error: ' + errorCode);
	};

	app.device.enableNotification(
		app.getScratchCharacteristicUUID(scratchBank),
		onSuccess,
		onFail
	);
};

app.readDataFromScratch = function(scratchNumber, successCallback, failCallback)
{
	if (app.connected)
	{
		console.log('Trying to read data from scratch ' + scratchNumber);
		app.device.readCharacteristic(
			app.getScratchCharacteristicUUID(scratchNumber),
			successCallback,
			failCallback);
	}
	else
	{
		console.log('Not connected to device, cant read data from scratch.');
	}
};

app.showInfo = function(info)
{
	console.log(info);
	document.getElementById('BLEStatus').innerHTML = info;
};