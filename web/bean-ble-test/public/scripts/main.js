"use strict";

(function () {
  
  // Global event bus
  var vent = {};
  _.extend(vent, Backbone.Events);

  window.DEBUG = {
    generateScore: function() {
      return {
        total:         parseInt(100*Math.random()),
        rampUp:        parseInt(15*Math.random()),
        impact:        parseInt(70*Math.random()),
        followThrough: parseInt(15*Math.random()),
      };
    },
    generateAccelData: function() {
      var accelData = [];
      var dataPoints = 50
      for(var i=0;i<dataPoints;i++) {
        var point = Math.random()*100;
        accelData.push(point);
      }
      return accelData;
    },
    generateData: function() {
      return {
        accelData: this.generateAccelData(),
        score:     this.generateScore()
      };
    },
    refreshTopScores: function () { vent.trigger('refreshTopScores');},
    updateScore: function (score) {
      score = score || this.generateScore();
      vent.trigger('updateScore', score);
    },
    updateChart: function(accelData) {
      accelData = accelData || this.generateAccelData();
      vent.trigger('updateChart', accelData);
    },
    mockData: function (data) {
      data = data || this.generateData();
      vent.trigger('dataReceived', data);
    },
  };

  // TODO: need to make private, extract out
  var lastScore = null;

  var chart = c3.generate({
    bindto: '#chart',
    data: {
      columns: [
        ['data1', 0],
      ],
      type: 'area-spline'
    }
  });

  // Global Events
  vent.on('dataReceived', function(data) {
    vent.trigger('updateScore', data.score);
    vent.trigger('updateChart', data.accelData);
  });

  vent.on('refreshTopScores', function () {
    refreshHighFiveList();
  });

  vent.on('updateScore', function (score) {
    lastScore = score.total;

    $("#score-total").transition('hide').html(score.total).transition('scale', 800);
    $("#ramp-up-score .value").transition('hide').html(score.rampUp+ "/15").transition('fade', 800);
    $("#impact-score .value").transition('hide').html(score.impact+ "/70" ).transition('fade', 800);
    $("#follow-through-score .value").transition('hide').html(score.followThrough+ "/15").transition('fade', 800);

    if(isHighScore(score.total)) vent.trigger('showHighScoreModal');
  });

  vent.on('showHighScoreModal', function(){
    $('#save-score-modal').modal('show');
  });

  vent.on('dataSaved', function() {
    $('#save-score-modal').modal('hide');
    vent.trigger('refreshTopScores');
  });

  vent.on('updateChart', function (accelData) {
    var columnData = ['data1'];
    
    for(var i=0; i<accelData.length; i++) {
      columnData.push(accelData[i]);
    }

    chart.load({
    columns: [
      columnData
    ]});
  });

  vent.trigger('refreshTopScores');

  var saveScoreFromForm = function (score) {
    var username = $('#save-score-modal input#save-score-input').val();
    db.saveHighScore({username: username, score: score});
    $('#save-score-modal input#save-score-input').val('')
    vent.trigger('dataSaved');
  };

  $("#save-score-input").keyup(function (e) {
    if (e.keyCode == 13) {
      saveScoreFromForm(lastScore);
    }
  });

  $("#save-score-input .save.button").click(function (e) {
    e.preventDefault();
    saveScoreFromForm(lastScore);
  });

  var socket = io('http://localhost:3000');

  socket.on('connect', function (data) {
    console.log("Connected to server...");
  });

  socket.on('beanConnected', function (beanName) {
    console.log("Bean device " + beanName + " connected!");
  });

  socket.on('score', function (data) {
    console.log("high five detected!")
    console.log(data);
    vent.trigger('dataReceived', data);
  });

  $('#reconnect-button').on('click', function() {
    socket.emit('restartBean');
  });

})();