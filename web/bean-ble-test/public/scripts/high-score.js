var isHighScore = function (score) {
  var highScores = db.getHighScores();
  if (highScores.length == 0) {
    return true;
  }
  if(score > highScores[highScores.length-1].score || highScores.length < 10 ) {
    return true;
  } else {
    return false;
  }
};

var highFiveItemTemplate = _.template(
  "<li class='high-score-item'> \
    <span class='username'><%- username %></span> \
    <span class='value'><%- score %></span> \
  </li>"
  );

var refreshHighFiveList = function () {
  var highScores = db.getHighScores();
  $highScoresEl = $('#high-scores-list');
  $highScoresEl.empty();
  $highScoresEl.transition('hide');
  for(var i=0;i<highScores.length; i++) {
    var username = highScores[i].username||'anonymous';
    var score = highScores[i].score;
    var itemHtml = highFiveItemTemplate({username: username, score: score});
    $highScoresEl.append(itemHtml);
  }
  $highScoresEl.transition('fade in', 800);
};
