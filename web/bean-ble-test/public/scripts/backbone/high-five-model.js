HighFive = Backbone.Model.extend({
  localStorage: new Backbone.LocalStorage("HighFiveCollection"),
  defaults: {
    username : 'anonymous',
    score    : 0
  },

  initialize: function() {
  },

  events: {

  }
});

HighFiveCollection = Backbone.Collection.extend({
  localStorage: new Backbone.LocalStorage("HighFiveCollection"),
  model: HighFive,

  // getTopHighFives

});