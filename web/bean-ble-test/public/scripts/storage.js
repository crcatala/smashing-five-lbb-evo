var db = (function () {
  var localStorageExists = (localStorage !== undefined);
  var storageKey = 'SmashingFive';
  var scores = [];
  var maxSize = 10;
  if (localStorageExists) {
    localStorage[storageKey] = localStorage[storageKey] || '[]'
  }

  if (localStorageExists) {
    scores = JSON.parse(localStorage[storageKey]);
  }

  var saveHighScore = function (score) {
    scores.push(score);
    scores.sort(function(a,b) {return b.score-a.score});
    if(scores.length > 10) { scores = scores.slice(0,10);}
    if (localStorageExists) {
      console.log("setting LS");
      localStorage[storageKey] = JSON.stringify(scores);
    }
  };

  var getHighScores = function () {
    return scores;
  };

  return {
    saveHighScore: saveHighScore,
    getHighScores: getHighScores,
  }
})();