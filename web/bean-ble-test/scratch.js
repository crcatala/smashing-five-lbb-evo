/*jslint node: true */
"use strict";

/* 
 * This script listens for data on the first two (of five available) Scratch characteristics
 * when this sketch is programmed to the Arduino:
 * https://punchthrough.com/bean/the-arduino-reference/setscratchdata/
 */

var Bean = require('ble-bean');
var evothings = {};
evothings.util = require('./evothings-util.js');

var intervalId;
var connectedBean;

Bean.discover(function(bean){
  console.log("Bean Discovered!")
  connectedBean = bean;
  process.on('SIGINT', exitHandler.bind(this));

  bean.on("serial", function(data, valid){
    console.log(data.toString());
  });

  bean.on("disconnect", function(){
    process.exit();
  });

  bean.connectAndSetup(function(){
    console.log("Bean connected and setup!");

    var scratch1LogEnabled = false;
    var scratch2LogEnabled = false;
    var scratch3LogEnabled = false;
    var scratch4LogEnabled = false;
    var scratch5LogEnabled = false;
    var scoreLogEnabled    = true;

    bean.notifyOne(
    function(data){
      if (scratch1LogEnabled) { console.log("Magnitude detected!");}
      if(data && data.length>=1){
        var accelData = new Uint8Array(data);
        for(var i=0; i<accelData.length; i++) {
          if (scratch1LogEnabled) { console.log(i+": ", accelData[i]);}
        }
      }
    },
    function(error){
      if(error) console.log("two setup: ", error);
    });

    bean.notifyTwo(
    function(data){
      if (scratch2LogEnabled) { console.log("X detected!");}
      if(data && data.length>=1){
        var accelData = new Uint8Array(data);
        for(var i=0; i<accelData.length; i++) {
          if (scratch2LogEnabled) { console.log(i+": ", accelData[i]);}
        }
      }
    },
    function(error){
      if(error) console.log("two setup: ", error);
    });

    var accelIdeal = 125.0;
    var scoreMax   = 100;

    bean.notifyThree(
    function(data){
      if(scratch3LogEnabled) { console.log("Y detected!");}
      if(data && data.length>=1){
        var accelData = new Uint8Array(data);
        for(var i=0; i<accelData.length; i++) {
          if(scratch3LogEnabled) { console.log(i+": ", accelData[i]);}
        }
        var accelScore = scoreMax * Math.exp(-Math.pow((accelData[9] - accelIdeal)/10,2)/(2*(16)));
        accelScore = parseInt(accelScore);
        if(scoreLogEnabled){ console.log("Score: ", accelScore);}
      }
    },
    function(error){
      if(error) console.log("two setup: ", error);
    });

    bean.notifyFour(
    function(data){
      if (scratch4LogEnabled) { console.log("Z detected!"); }
      if(data && data.length>=1){
        var accelData = new Uint8Array(data);
        for(var i=0; i<accelData.length; i++) {
          if (scratch4LogEnabled) { console.log(i+": ", accelData[i]); }
        }
      }
    },
    function(error){
      if(error) console.log("two setup: ", error);
    });

    bean.notifyFive(
    function(data){
      if(scratch5LogEnabled) { console.log("#5 detected!");}
      if(data && data.length>=1){
        var accelData = new Uint8Array(data);
        for(var i=0; i<accelData.length; i++) {
          if(scratch5LogEnabled) { console.log(i+": ", accelData[i]);}
        }
      }
    },
    function(error){
      if(error) console.log("two setup: ", error);
    });

  });

});

process.stdin.resume();//so the program will not close instantly
var triedToExit = false;

//turns off led before disconnecting
var exitHandler = function exitHandler() {

  var self = this;
  if (connectedBean && !triedToExit) {
    triedToExit = true;
    console.log('Turning off led...');
    clearInterval(intervalId);
    connectedBean.setColor(new Buffer([0x0,0x0,0x0]), function(){});
    //no way to know if succesful but often behind other commands going out, so just wait 2 seconds
    console.log('Disconnecting from Device...');
    setTimeout(connectedBean.disconnect.bind(connectedBean, function(){}), 2000);
  } else {
    process.exit();
  }
};