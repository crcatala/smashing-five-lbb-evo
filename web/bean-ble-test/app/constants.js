"use strict";

var constants = function () {
  
  global.EASY_DIFFICULTY   = 'easy';
  global.NORMAL_DIFFICULTY = 'normal';
  global.HARD_DIFFICULTY   = 'hard';

};

module.exports = constants;