"use strict";

var ScratchBankTracker = require('./scratch-bank-tracker.js');

var beanCode = function(vent, config) {
  config = config || {};

  var Bean = require('ble-bean');
  var evothings = {};
  evothings.util = require('./evothings-util.js');

  var accelData = new Array([],[],[],[],[]);

  var getBeanName = function(device){
    if (device._peripheral) {
      return device._peripheral.advertisement.localName;
    } else if(device.advertisement) {
      return device.advertisement.localName;
    } else {
      return null;
    }
  };

  var connectedBean;

  var onDiscover = function(bean){
    debugger;
    var intervalId;
    
    var triedToExit = false;

    var beanName = getBeanName(bean);
    console.log("Bean Discovered!", beanName)

    connectedBean = bean;
    process.once('SIGINT', exitHandler.bind(this));

    bean.once("disconnect", function(){
      if (triedToExit) {
        process.exit();
      } else {
        // reconnect?
      }
    });

    var checkBanksFull = function () {
        if(bean.scratchBanks.isFull()){
          vent.emit('dataReceivedFromBean', bean.scratchBanks.getCombinedData(), beanName);
          bean.scratchBanks.reset();
        }
      };

      var beanNotifySuccess = function(scratchBank){
        return function (data) {
          vent.emit('notificationReceived', scratchBank);
          if (config.scratchLogEnabled) { console.log("Scratch Bank "+ scratchBank +" detected!");}
          if(data && data.length>=1){
            // TODO: is all this array conversion necessary
            var arr = new Uint8Array(data);
            var readableData = new Array();
            for(var i=0; i<arr.length; i++) {
              readableData[i] = arr[i];
              if (config.scratchLogEnabled) { console.log(i+": ", readableData[i]);}
            }
            bean.scratchBanks.set(scratchBank, readableData);
          }
          checkBanksFull(); 
        };
      };

      var beanNotifyError = function(scratchBank) {
        return function(error){
          if(error) console.log("Scratch bank " + scratchBank + " error:", error);
        };
      };

      var notificationListeners = [
        bean.notifyOne,
        bean.notifyTwo,
        bean.notifyThree,
        bean.notifyFour,
        bean.notifyFive
      ];

      var listenForNotificationsOnBank =  function(scratchBank) {
        var successCallback = beanNotifySuccess(scratchBank);
        var errorCallback = beanNotifyError(scratchBank);
        notificationListeners[scratchBank-1].call(bean, successCallback, errorCallback);       
      };

    bean.connectAndSetup(function(){
      console.log(beanName + " connected and setup!");
      vent.emit('beanConnected', beanName);

      bean.scratchBanks = new ScratchBankTracker();
      
      listenForNotificationsOnBank(1);
      listenForNotificationsOnBank(2);
      listenForNotificationsOnBank(3);
      listenForNotificationsOnBank(4);
      listenForNotificationsOnBank(5);

    });

    //turns off led before disconnecting
    function exitHandler() {

      var self = this;
      if (connectedBean && !triedToExit) {
        triedToExit = true;
        console.log('Turning off led on '+ beanName + '...');
        clearInterval(intervalId);
        connectedBean.setColor(new Buffer([0x0,0x0,0x0]), function(){});
        //no way to know if succesful but often behind other commands going out, so just wait 2 seconds
        console.log('Disconnecting from '+ beanName + '...');
        setTimeout(connectedBean.disconnect.bind(connectedBean, function(){}), 2000);
      } else {
        process.exit();
      }
    };

  };

  var stopDiscovery = function() {
    setTimeout(function(){
      Bean.stopDiscoverAll(onDiscover);
      console.log("Stopped autodiscovery");
    }, config.stopDiscoveryTimeout);
  };

  var restartBeanCallback = function() {
    console.log("backend received restartBean");
    connectedBean.disconnect(function(error) {
      console.log("disconnect callback");
      console.log("disconnect callback error", error);
    });
    console.log("flag 1 disconnected");
    setTimeout(function() { 
      Bean.discoverAll(onDiscover);
      stopDiscovery();
    }, 3000);
    // listenForNotificationsOnBank(1);
    // listenForNotificationsOnBank(2);
    // listenForNotificationsOnBank(3);
    // listenForNotificationsOnBank(4);
    // listenForNotificationsOnBank(5);
    console.log("flag 2 should be connected again");
  };

  vent.on('restartBean', restartBeanCallback);

  Bean.discoverAll(onDiscover);
  stopDiscovery();

  process.stdin.resume();//so the program will not close instantly

};

module.exports = beanCode;