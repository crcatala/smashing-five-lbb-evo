"use strict";

var scoreCalculator = function (config) {

config = config || {}

// 
// Scoring Config
// 
var scoreMax            = config.scoreMax || 100;
var rampUpIdeal         = config.rampUpIdeal || 180.0;
var impactIdeal         = config.impactIdeal || 350.0;
var followThroughIdeal  = config.followThroughIdeal || 100.0;
var rampUpWeight        = config.rampUpWeight || scoreMax*0.15;
var impactWeight        = config.impactWeight || scoreMax*0.70;
var followThroughWeight = config.followThroughWeight || scoreMax*0.15;
var difficulty          = config.difficulty || NORMAL_DIFFICULTY;
var difficultyFactor;
switch(difficulty) {
  case EASY_DIFFICULTY:
    difficultyFactor = 32;
    break;
  case NORMAL_DIFFICULTY:
    difficultyFactor = 16;
    break;
  case HARD_DIFFICULTY:
    difficultyFactor = 8;
    break;
  default:
    difficultyFactor = 16;
} 

// 
// Score takes into account three phases (and absolute peak)
// 1) Ramp-Up
// 2) Impact
// 3) Follow-Through
// 
var sumOfArray = function (arr) {
  return arr.reduce(function(pv, cv) { return pv + cv; }, 0);
}

var calculateScore = function (data) {
  var centerIndex       = 49;
  var rampUpData        = data.slice(centerIndex-7, centerIndex-2);
  var impactData        = data.slice(centerIndex-2, centerIndex+3);
  var followThroughData = data.slice(centerIndex+3, centerIndex+8);

  var rampUpScore = rampUpWeight*Math.exp(-Math.pow((sumOfArray(rampUpData) - rampUpIdeal)/(rampUpIdeal/15),2)/(2*(difficultyFactor)));
  var impactScore = impactWeight*Math.exp(-Math.pow((sumOfArray(impactData) - impactIdeal)/(impactIdeal/20),2)/(2*(difficultyFactor)));
  var followThroughScore = followThroughWeight*Math.exp(-Math.pow((sumOfArray(followThroughData) - followThroughIdeal)/(followThroughIdeal/15),2)/(2*(difficultyFactor)));

  rampUpScore = parseInt(rampUpScore);
  impactScore = parseInt(impactScore);
  followThroughScore = parseInt(followThroughScore);

  var score = rampUpScore + impactScore + followThroughScore;
  score = parseInt(score);
  
  if (config.scoreLogEnabled) {
    console.log("ramp Sum", sumOfArray(rampUpData));
    console.log("impact Sum", sumOfArray(impactData));
    console.log("followThrough Sum", sumOfArray(followThroughData));
    console.log("rampUpScore", rampUpScore);
    console.log("impactScore", impactScore);
    console.log("followThroughScore", followThroughScore);
    console.log("total score", score);
  }

  return { 
    total: score,
    rampUp: rampUpScore,
    impact: impactScore,
    followThrough: followThroughScore
  };
};

return {calculate: calculateScore};

} // end scoreCalculator function

module.exports = scoreCalculator;