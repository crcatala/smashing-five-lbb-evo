var ScratchBankTracker = function () {
  this.banks = new Array(5);
};

ScratchBankTracker.prototype.get = function (num) {
  return this.banks[num-1];
};

ScratchBankTracker.prototype.set = function (num, data) {
  this.banks[num-1] = data;
  return this.banks[num-1];
};

ScratchBankTracker.prototype.hasData = function (num) {
  return (this.get(num) !==  undefined);
};

ScratchBankTracker.prototype.isFull = function () {
  var result = true;
  for (var i=0; i<this.banks.length; i++) {
    if (!this.banks[i]) {
      result = false;
      break;
    }
  }
  return result;
};

ScratchBankTracker.prototype.getCombinedData = function () {
  var result = [];
  for (var i=0; i<this.banks.length; i++) {
    if(this.banks[i]) result = result.concat(this.banks[i]);
  }
  return result;
};

ScratchBankTracker.prototype.reset = function () {
  for (var i=0; i<this.banks.length; i++) {
    this.banks[i] = null;
  }
};

module.exports = ScratchBankTracker;