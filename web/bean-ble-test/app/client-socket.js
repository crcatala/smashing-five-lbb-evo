"use strict";

var socketCode = function (server, vent) {
  var io = require('socket.io')(server);


  io.on('connection', function (socket) {
    var clientDataReadyCallback = function (data) {
      socket.emit('score', data);
    };

    var beanConnectedCallback = function (beanName) {
      socket.emit('beanConnected', beanName);
    };
    
    console.log("client connected...");

    socket.emit('connect');

    vent.on('clientDataReady', clientDataReadyCallback);

    vent.on('beanConnected', beanConnectedCallback);

    socket.on('restartBean', function () {
      console.log("reconnecting bean...");
      // vent.removeListener('clientDataReady', clientDataReadyCallback);
      // vent.removeListener('beanConnected', beanConnectedCallback);
      vent.emit('restartBean');
      // vent.removeListener('clientDataReady', clientDataReadyCallback);
      // vent.removeListener('beanConnected', beanConnectedCallback);
    });

    socket.on('disconnect', function () {
      console.log("client disconnected...");
      vent.removeListener('clientDataReady', clientDataReadyCallback);
      vent.removeListener('beanConnected', beanConnectedCallback);
    });

  });

};

module.exports = socketCode;