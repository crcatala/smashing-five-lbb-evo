"use strict";

require('./app/constants.js')();

// 
// SmashingFive Config
// 

var appConfig = {
  difficulty:            EASY_DIFFICULTY,
  scratchLogEnabled:     true,
  scoreLogEnabled:       true,
  stopDiscoveryTimeout:  10000,
  scoreMax:              100,
  rampUpIdeal:           110.0,
  impactIdeal:           300.0,
  followThroughIdeal:    80.0,
  rampUpWeight:          15,
  impactWeight:          70,
  followThroughWeight:   15,
};

var server = require('./app/routes.js');
var events = require('events');
var vent   = new events.EventEmitter();
var scoreCalculator = require('./app/score-calculator.js')(appConfig);
var clientSocket    = require('./app/client-socket.js')(server,vent, appConfig);
var beanDevice      = require('./app/bean-device.js')(vent, appConfig);

// 
// Triggered by bean-device.js when all five banks received
// 
vent.on('dataReceivedFromBean', function (data, beanName) {
  console.log("Complete data received from bean");
  var score = scoreCalculator.calculate(data);
  var clientData = { name: beanName, score: score, accelData: data };
  vent.emit('clientDataReady', clientData);
});